#ifndef MAP_H
#define MAP_H

#include "main.h"
#include "room.h"

class ClassMap
{
private:
	int currentLocation;
	int numberOfRooms;
	// pointer to array initialised in constructor
	ClassRoom *roomArray;

public:
	// Constructors and Destructors
	ClassMap();
	~ClassMap();

	int getCurrentRoomID ();

	// Print description, events and directions for current room
	std::string getCurrentRoom ();

	// Change currentLocation to neighbour of current room if possible
	int moveRoom (char direction);


	// self explanitory
	int getNumberOfRooms ();

	// dump room descriptions to command line (debugging)
	int printRoomDescriptions ();

};

#endif