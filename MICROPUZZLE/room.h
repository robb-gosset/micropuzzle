#ifndef ROOM_H
#define ROOM_H

#include "main.h"

class ClassRoom
{
private:
	// ints to store ID's of neighbouring rooms (0 means no neighbour)
	int northNeighbour;
	int eastNeighbour;
	int southNeighbour;
	int westNeighbour;
	char roomDescription[79];

public:

	// Constructor and Destructor - single setter used for initialisation as data is loaded from file
	ClassRoom ();

	~ClassRoom ();

	int initialise( int newNorthNeighbour = 0, 
				int newEastNeighbour = 0, 
				int newSouthNeighbour = 0,	
				int newWestNeighbour = 0, 
				char newRoomDescription[79] = 0);

	// Return room description as string
	std::string getDescription ();

	// Return present neighbours as string
	std::string getMoves ();


	// Return new room ID to move to neighbour

/******************************************
 * return >   0 - new room ID
 * return ==  0 - no neighbour in direction specified
 * return == -1 - invalid direction
 * return <  -1 - How the fuck did you get this?
 ******************************************/
	int move (char direction = ' ');

};

#endif