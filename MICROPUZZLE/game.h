#ifndef GAME_H
#define GAME_H

#include "main.h"

#include "inventory.h"
#include "map.h"

class ClassGame
{
private:
ClassMap *gameMap;
ClassInventory *gameInventory;
public:
	ClassGame();
	~ClassGame();

	int printMapContents ();

};

#endif