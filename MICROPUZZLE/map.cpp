#include "map.h"
#include "room.h"

#include "main.h"

using namespace std;

// Constructor for map
/***********************************************************************
* Function breakdown
* 1 - Find number of rooms stored in map.csv
* 2 - Allocate memory for that number of rooms
* 3 - Read each room into temporary memory then invoke constructor for each room
* 4 - ??????
* 5 - Profit
* 6 - Ready to map that shit
***********************************************************************/


ClassMap::ClassMap ()
{
	numberOfRooms = 0;

	// Get number of rooms in map.csv
	
		unsigned int number_of_lines = 0;
		FILE *infile = fopen("map.csv", "r");
		int ch;

		while (EOF != (ch=getc(infile)))
			if ('\n' == ch)
				++number_of_lines;
		fclose(infile);
		numberOfRooms = number_of_lines;
	



	// allocate memory for rooms array
	roomArray = new ClassRoom[numberOfRooms +1];

	// set starting room
	int currentLocation = 1;

	// load that shit up
	{
		// Holders for values read from file
		int newRoomID = 0;
		char newRoomDescription[79] = "";
		int newRoomNorthNeighbour = 0;
		int newRoomEastNeighbour = 0;
		int newRoomSouthNeighbour = 0;
		int newRoomWestNeighbour = 0;

		// used for iterations
		int i = 0;

		// File stream for map.csv
		std::ifstream mapFile;

		// Crack that shit open
		mapFile.open ("map.csv");

		// Line buffer for parsing
		std::string line;


		// For each line in the map.csv file read in the values into variables declared above then run initialise function for each room to store values into array
		while (newRoomID < numberOfRooms)
		{
			// Read next line from map.csv
			std::getline(mapFile, line);

			// re-init parameters

			newRoomID = 0;
			newRoomNorthNeighbour = 0;
			newRoomEastNeighbour = 0;
			newRoomSouthNeighbour = 0;
			newRoomWestNeighbour = 0;
			for(i = 0;i<79;i++)
			{
				newRoomDescription[i] = ' ';
			}


			int parameter = 0;

			int paraStart = 0;
			int paraEnd = 0;

			std::string buffer;
			std::istringstream iss(line);

			for(parameter = 0; parameter <= 5; parameter++)
			{
				// Empty buffer from last iteration
				buffer.clear();

				// Find end of current parameter
				paraEnd = line.find(',',paraStart+1);

				/***************************************
				 * Parameter Values
				 * 0 - roomID
				 * 1 - roomDescription
				 * 2 - roomNorthNeighbour
				 * 3 - roomEastNeighbour
				 * 4 - roomSouthNeighbour
				 * 5 - roomWestNeighbour
				 **************************************/
				switch (parameter)
				{
				case 0: 
					buffer = line.substr((paraStart),(paraEnd-paraStart));
					newRoomID = atoi(buffer.c_str());
					break;
				case 1:
					buffer = line.substr((paraStart+2),(line.find("\"",paraStart+2)-(paraStart+2)));
					for(i = 0;i<(buffer.length());i++)
					{
						newRoomDescription[i] = buffer.c_str()[i];
					}
					break;
				case 2:
					buffer = line.substr((paraStart+1),(paraEnd-paraStart));
					newRoomNorthNeighbour = atoi(buffer.c_str());
					break;
				case 3:
					buffer = line.substr((paraStart+1),(paraEnd-paraStart));
					newRoomEastNeighbour = atoi(buffer.c_str());
					break;
				case 4:
					buffer = line.substr((paraStart+1),(paraEnd-paraStart));
					newRoomSouthNeighbour = atoi(buffer.c_str());
					break;
				case 5:
					buffer = line.substr((paraStart+1),(paraEnd-paraStart));
					newRoomWestNeighbour = atoi(buffer.c_str());
					break;
				} // switch

				// Cycle paraEnd to paraStart
				paraStart = paraEnd;

			} // for parameters loop

			// Init next room with data
			roomArray[newRoomID].initialise(newRoomNorthNeighbour,
				newRoomEastNeighbour,
				newRoomSouthNeighbour,
				newRoomWestNeighbour,
				newRoomDescription);

		} // while !EOF
		// Close the file because we're a good little program and we don't need that shit no more
		// Causes exception because of heap error?
		mapFile.close();
	}
}

ClassMap::~ClassMap()
{
	// invoke destructors
	for (int i = 0; i < numberOfRooms; i++ )
		roomArray[i].~ClassRoom();

	// deallocate memory
	delete[] roomArray;
}



// Return description of current room and possible directions to travel
std::string ClassMap::getCurrentRoom()
{
	std::string buffer = "You Are ";
	buffer.append(roomArray[currentLocation].getDescription());
	// ----------------------------------------------------------------------------------TODO EVENTS!!!!!!!
	buffer.append ("\n You can go ");
	buffer.append (roomArray[currentLocation].getMoves());
	return buffer;
}

// self explanatory
int ClassMap::getCurrentRoomID ()
{
	return currentLocation;
}

// Move to new room - expects one of nNeEsSwW, will return numbers if not in this set
int ClassMap::moveRoom(char direction)
{

	int buffer = 0;
	buffer = roomArray[currentLocation].move(direction);
	/******************************************
	* Possible values for buffer now
	* return >   0 - new room ID
	* return ==  0 - no neighbour in direction specified
	* return == -1 - invalid direction
	* return <  -1 - How the fuck did you get this?
	******************************************/
	if (buffer > 0)
	{
		// Valid move, change location
		currentLocation = buffer;
		return 0;
	} else if (buffer == 0)
	{
		// No neighbour, stay here, return 1 to show bad move
		return 1;
	} else if (buffer == -1)
	{
		// Invalid direction, return 2 to show bad direction
		return 2;
	} else
	{
		// Somehow got a value < -1, shouldn't ever reach here so might as well delete System32 or something... now how to do that?
		return 9001;
	}

	// Shouldn't reach here
	return 0;
}

// self explanatory
int ClassMap::getNumberOfRooms()
{
	return numberOfRooms;
}

// dump room descriptions to std::cout for debugging
int ClassMap::printRoomDescriptions ()
{
	for(int j = this->getNumberOfRooms() - 1; j > 0; j--)
	{
		cout << roomArray[j].getDescription();
	}
	return 0;
}