#include "game.h"

#include "main.h"

using namespace std;

ClassGame::ClassGame()
{
	ClassMap *gameMap = new ClassMap();
	ClassInventory *gameInventory = new ClassInventory();
}

ClassGame::~ClassGame()
{
	delete gameMap;
	delete gameInventory;
}

int ClassGame::printMapContents()
{
	gameMap->printRoomDescriptions();
	return 0;
}