#include "item.h"
#include "main.h"

ClassItem::ClassItem (	int newItemID, 
						char newItemDescription[79], 
						int newItemLocation)
{
	itemID = newItemID;
	itemLocation = newItemLocation;
	for(int i = 0;i < (strlen(newItemDescription)-1);i++)
	{
		itemDescription[i] = newItemDescription[i];
	}
}

ClassItem::~ClassItem ()
{

}

int ClassItem::initialise(int newItemID,
	char newItemDescription[79],
	int newItemLocation)
{
	itemID = newItemID;
	itemLocation = newItemLocation;
	for (int i = 0; i < (strlen(newItemDescription) - 1); i++)
	{
		itemDescription[i] = newItemDescription[i];
	}
	return 0;
}

std::string ClassItem::getItemDescription ()
{
	return itemDescription;
}

int ClassItem::moveItem (int newLocationID)
{
	return 0;
}