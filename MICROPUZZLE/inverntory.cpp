#include "inventory.h"

#include "main.h"

using namespace std;


ClassInventory::ClassInventory()
{
	numberOfItems = 0;

	// Get number of rooms in items.csv

	unsigned int number_of_lines = 0;
	FILE *infile = fopen("items.csv", "r");
	int ch;

	while (EOF != (ch = getc(infile)))
	if ('\n' == ch)
		++number_of_lines;
	fclose(infile);
	numberOfItems = number_of_lines;




	// allocate memory for rooms array
	itemArray = new ClassItem[numberOfItems + 1];

	// load that shit up
	{
		// Holders for values read from file
		int newItemID = 0;
		char newItemDescription[79] = "";
		int newItemLocation = 0;

		// used for iterations
		int i = 0;

		// File stream for map.csv
		std::ifstream itemFile;

		// Crack that shit open
		itemFile.open("items.csv");

		// Line buffer for parsing
		std::string line;


		// For each line in the items.csv file read in the values into variables declared above then run initialise function for each room to store values into array
		while (newItemID < numberOfItems)
		{
			// Read next line from items.csv
			std::getline(itemFile, line);

			// re-init parameters

			newItemID = 0;
			newItemLocation = 0;
			for (i = 0; i<79; i++)
			{
				newItemDescription[i] = ' ';
			}


			int parameter = 0;

			int paraStart = 0;
			int paraEnd = 0;

			std::string buffer;
			std::istringstream iss(line);

			for (parameter = 0; parameter <= 2; parameter++)
			{
				// Empty buffer from last iteration
				buffer.clear();

				// Find end of current parameter
				paraEnd = line.find(',', paraStart + 1);

				/***************************************
				* Parameter Values
				* 0 - itemID
				* 1 - itemDescription
				* 2 - itemLocation
				**************************************/
				switch (parameter)
				{
				case 0:
					buffer = line.substr((paraStart), (paraEnd - paraStart));
					newItemID = atoi(buffer.c_str());
					break;
				case 1:
					buffer = line.substr((paraStart + 2), (line.find("\"", paraStart + 2) - (paraStart + 2)));
					for (i = 0; i<(buffer.length()); i++)
					{
						newItemDescription[i] = buffer.c_str()[i];
					}
					break;
				case 2:
					buffer = line.substr((paraStart + 1), (paraEnd - paraStart));
					newItemLocation = atoi(buffer.c_str());
					break;
				} // switch

				// Cycle paraEnd to paraStart
				paraStart = paraEnd;

			} // for parameters loop

			// Init next item with data
			itemArray[newItemID].initialise(newItemID,
				newItemDescription,
				newItemLocation);

		} // while !EOF
		// Close the file because we're a good little program and we don't need that shit no more
		// Causes exception because of heap error?
		itemFile.close();
	}
}

	ClassInventory::~ClassInventory()
	{

	}