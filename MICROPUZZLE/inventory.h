#ifndef INVENTORY_H
#define INVENTORY_H

#include "item.h"
#include "main.h"

class ClassInventory
{
private:
	int numberOfItems = 0;
	ClassItem *itemArray;

public:

	// Constructor and Destructor
	ClassInventory();
	~ClassInventory();

};

#endif