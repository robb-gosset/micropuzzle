#include "room.h"

#include "main.h"

using namespace std;

// Public constructor and destructor
ClassRoom::ClassRoom (	)
{
	northNeighbour	= 0;
	eastNeighbour	= 0;
	southNeighbour	= 0;
	westNeighbour	= 0;
		for(int i = 0;i<79;i++)
		{
			roomDescription[i] = ' ';
		}

}

	ClassRoom::~ClassRoom()
{
		
}

	// Initialiser
	int ClassRoom::initialise (	int newNorthNeighbour, 
						int newEastNeighbour, 
						int newSouthNeighbour,	
						int newWestNeighbour, 
						char newRoomDescription[79])
{
	northNeighbour	= newNorthNeighbour;
	eastNeighbour	= newEastNeighbour;
	southNeighbour	= newSouthNeighbour;
	westNeighbour	= newWestNeighbour;
		for(int i = 0;i<79;i++)
		{
			roomDescription[i] = newRoomDescription[i];
		}
	return 0;
}


	// Return room description as string
std::string ClassRoom::getDescription ()
{
	std::string buffer;
	buffer.copy(roomDescription,79,0);
	return buffer;
}

	// Print present neighbours to std out
std::string ClassRoom::getMoves ()
{
	bool singleChar = 1;
	std::string buffer = "";

	if (northNeighbour)
	{
		buffer.append("N");
		singleChar = 0;
	} else {
		// Do Nothing
	}

	if (eastNeighbour)
	{
		// If buffer is not empty add ", "
		if (!buffer.empty())
		{
		buffer.append(", ");
		}
		buffer.append("E");
	} else {
		// Do Nothing
	}

	if (southNeighbour)
	{
		// If buffer is not empty add ", "
		if (!buffer.empty())
		{
			buffer.append(", ");
		}
		buffer.append("S");
	} else {
		// Do Nothing
	}

	if (westNeighbour)
	{
		// If buffer is not empty add ", "
		if (!buffer.empty())
		{
			buffer.append(", ");
		}
		buffer.append("W");
	} else {
		// Do Nothing
	}

	// Return contents of buffer.
	return buffer;
}

	// Return new room ID to move to neighbour

/******************************************
 * return >   0 - new room ID
 * return ==  0 - no neighbour in direction specified
 * return == -1 - invalid direction
 * return <  -1 - How the fuck did you get this?
 ******************************************/

int ClassRoom::move (char direction)
{
	// Return neighbour for direction
	switch (direction)
	{
	case 'N' :
	case 'n' :
		return northNeighbour;
		break;

	case 'E' :
	case 'e' :
		return eastNeighbour;
		break;

	case 'S' :
	case 's' :
		return southNeighbour;
		break;

	case 'W' :
	case 'w' :
		return westNeighbour;
		break;

	default:
		return -1;
		break;
	}
	return -1; // Should never be reached
}