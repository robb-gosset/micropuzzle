#include "main.h"

class ClassItem {
private:
	int itemID;
	char itemDescription[79];
	int itemLocation;

public:
	ClassItem (	int newItemID = 0, 
				char newItemDescription[79] = "", 
				int newItemLocation = 0);
	~ClassItem ();

	int initialise(	int newItemID = 0, 
					char newItemDescription[79] = "                                                                              ", 
					int intnewItemLocation = 0); 

	std::string getItemDescription ();
	int moveItem (int newLocationID);


};